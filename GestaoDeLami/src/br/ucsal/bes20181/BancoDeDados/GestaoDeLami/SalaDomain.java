package br.ucsal.bes20181.BancoDeDados.GestaoDeLami;

import java.util.ArrayList;

public class SalaDomain {

	private String nome;
	private Integer numeroComputadores;
	private Integer quantidadePessoas;
	private Integer id;
	static Integer idGlobal = 1;
	static ArrayList<SalaDomain> listaDeSalas = new ArrayList<>();

	public SalaDomain(String nome, Integer numeroComputadores, Integer quantidadePessoas) {
		this.nome = nome;
		this.numeroComputadores = numeroComputadores;
		this.quantidadePessoas = quantidadePessoas;
		this.id = idGlobal++;

	}

	public void status() {
		for (SalaDomain id : listaDeSalas) {
			System.out.println("--------- ID: " + id.getId() + " -----------");
			System.out.println("Nome da Sala: " + id.getNome());
			System.out.println("Numero de Computadores: " + id.getNumeroComputadores());
			System.out.println("Capacidade de Pessoas: " + id.getQuantidadePessoas());
			System.out.println("----------------------------");
		}

	}

	public static void adicionarSalaNaLista(SalaDomain sala) {
		boolean existe = false;
		for (SalaDomain salaDomain : listaDeSalas) {
			if (sala.getNome().equals(salaDomain.getNome())) {
				System.out.println("Impossivel adicionar essa sala, pois ja foi adicionada");
				existe = false;
			}
		}
		if (!existe) {
			listaDeSalas.add(sala);
			System.out.println("Sala adicionada na lista");
		}
	}

	public static void removerSala(Integer id) {

		for (SalaDomain remove : listaDeSalas) {
			if (remove.getId() == id) {
				listaDeSalas.remove(remove);
				System.out.println("Sala removida com sucesso!!");

			}

		}
	}

	public Integer getNumeroComputadores() {
		return numeroComputadores;
	}

	public void setNuemeroComputadores(Integer numeroComputadores) {
		this.numeroComputadores = numeroComputadores;

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;

	}

	public Integer getQuantidadePessoas() {
		return quantidadePessoas;
	}

	public void setQuantidadePessoas(Integer quantidadePessoas) {
		this.quantidadePessoas = quantidadePessoas;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
