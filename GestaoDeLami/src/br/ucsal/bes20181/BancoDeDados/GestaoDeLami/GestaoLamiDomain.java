package br.ucsal.bes20181.BancoDeDados.GestaoDeLami;

import java.util.Scanner;

public class GestaoLamiDomain {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		int opcao;

		do {
			System.out.println("Digite 1 para adicionar a sala");
			System.out.println("Digite 2 para editar a sala");
			System.out.println("Digite 3 para remover a sala");
			opcao = sc.nextInt();

			if (opcao == 1) {
				opcaoUm();
			} else if (opcao == 2) {

			}

		} while (opcao != 3);

	}

	static void opcaoUm() {
		System.out.println("Insira o nome da sala: ");
		String nome = sc.next();
		System.out.println("Insira a quantidade de computadores: ");
		int qtdComputadores = sc.nextInt();
		System.out.println("Insira a capacidade da sala: ");
		int capacidadeSala = sc.nextInt();
		SalaDomain sala = new SalaDomain(nome, qtdComputadores, capacidadeSala);
		System.out.println();
		SalaDomain.adicionarSalaNaLista(sala);
		System.out.println();
		sala.status();
		System.out.println();

	}

}